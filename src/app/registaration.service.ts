import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmailValidator } from '@angular/forms';
import { of, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistarationService {

constructor(private http: HttpClient) { }
 private isLoggedInSource = new BehaviorSubject(status);
 isLoggedIn = this.isLoggedInSource.asObservable();
  updateLogging(status) {
    this.isLoggedInSource.next(status);
  }
  getUserData(param) {
    
    return this.http.post("http://localhost:3000/companys/api/user", param)
  }
  

  postUserData(param) {
    let fd = new FormData;
    fd.append('img', param.avatar);
    fd.append('data', JSON.stringify(param));
    
    return this.http.post("http://localhost:3000/companys/api/data", fd)
  }

}
