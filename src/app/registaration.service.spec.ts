import { TestBed } from '@angular/core/testing';

import { RegistarationService } from './registaration.service';

describe('RegistarationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegistarationService = TestBed.get(RegistarationService);
    expect(service).toBeTruthy();
  });
});
