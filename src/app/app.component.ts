import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormControl, ReactiveFormsModule, FormGroup, Validators } from '@angular/forms';
import { RegistarationService } from './registaration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {



  constructor(private registaration : RegistarationService, private router: Router) { }
  registarationform = new FormGroup({
    "email": new FormControl(null, [
      Validators.required,
      Validators.email]),
    "companyname": new FormControl(null, [Validators.required]),
    "roomnumber": new FormControl(null, [Validators.required]),
    "avatar": new FormControl(null, [Validators.required]),
    "password": new FormControl(null, [Validators.required]),
    "confirmationpassword": new FormControl(null, [Validators.required]),
    "checkbox": new FormControl(null, [Validators.required]),
  });


  LogInForm = new FormGroup({
    "email": new FormControl(null, [Validators.required, Validators.email]),
    "password": new FormControl(null, [Validators.required]),
    "checkbox": new FormControl(null, [Validators.required]),
  });

  selectedimg;
  fd: FormData
  getAvatar(event) {
    console.log(event)
    this.selectedimg = event.target.files[0];

    this.onSelectFile(event);
    this.imgTitle()
    return this.selectedimg;
  }

  url;
  onSelectFile(event) {
    if(event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); 

      reader.onload = (event: any) => {
        this.url = event.target.result;
      }
    }
  }

  companynm;
  imgTitle() {
    this.companynm = this.registarationform.value.companyname;
    return this.companynm;
  }
  
  confirmation;
  confirmpassword() {
    
    const pass = this.registarationform.value.password;
    const confirmpass = this.registarationform.value.confirmationpassword;
    
    if(pass !== confirmpass) {
      this.confirmation = 'please input the confirmation of password'
    }if(pass === confirmpass) {
      this.confirmation = 'confirmed'
    }
    return this.confirmation;
  }
  

  logInGet() {
    return this.LogInForm.value
  }


  save() {
    this.registarationform.value.avatar = this.selectedimg;
    console.log(this.selectedimg)
    return this.registarationform.value
  }
  
  
  dataColl() {
    return this.registarationform.value;
  }



  checked = true;
  signUp() {
    this.checked = true;
    console.log(this.checked);
    return this.checked;
  }

  logIn() {
    this.checked = false;
    console.log(this.checked);
    return this.checked;
  }
  
  ngOnInit() {
  }
 
  
    
  
  sendData() {
    this.registaration.postUserData(this.dataColl()).subscribe(res => {
      console.log(res);
      if(res) {
        console.log('welcome to our comunity!');
        return this.registaration.updateLogging(true);
    }else {
      console.log('this user is already exist!')
      return this.registaration.updateLogging(false);
    }
  }) 
}

getData() {
  this.registaration.getUserData(this.logInGet()).subscribe(res => {
    if(res) {
      console.log('you are a fucking user you can enter the fucking website'+ "your token is ::::..."+ res);
      this.router.navigate(["/welcome"])
      return this.registaration.updateLogging(true);      
    }
    else {
      console.log("Are you fucking crazy you are not allowed to enter there please login with a ligal user")
      return this.registaration.updateLogging(false);
    }
  })
}

}
