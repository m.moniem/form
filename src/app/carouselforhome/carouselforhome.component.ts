import { Component, OnInit,  } from '@angular/core';



@Component({
  selector: 'app-carouselforhome',
  templateUrl: './carouselforhome.component.html',
  styleUrls: ['./carouselforhome.component.css']
})
export class CarouselforhomeComponent implements OnInit {

  images = ["../../assets/carousel1.jpg", "../../assets/carousel2.jpg", "../../assets/carousel3.jpg"]
  
  constructor() { }

  ngOnInit() {
  }

}
