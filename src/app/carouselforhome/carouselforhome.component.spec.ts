import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselforhomeComponent } from './carouselforhome.component';

describe('CarouselforhomeComponent', () => {
  let component: CarouselforhomeComponent;
  let fixture: ComponentFixture<CarouselforhomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselforhomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselforhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
